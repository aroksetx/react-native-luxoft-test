import React, { Component } from 'react'
import { Text, ScrollView } from 'react-native'
import LocationsListItem from './LocationsListItem';

export default class LocationList extends Component {
    render() {
        let coordinates = {
            latitude: 23.33,
            longitude: -42.22
        }

        return (
            <ScrollView>
                <LocationsListItem title="Los Angeles" coordinates={coordinates}/>
            </ScrollView>
        )
    }
}