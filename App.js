/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import AppNavigator from './src/AppNavigator';
import { getLocationsList } from './src/services/LocationsService'
import { Provider } from 'react-redux';
import store from './src/store';

export default class App extends Component {
    constructor() {
        super();
    }

    componentDidMount() {
        getLocationsList().then(({locations}) => console.log(locations));
    }

    render() {
        return (
            <Provider store={store}>
                <AppNavigator/>
            </Provider>
        );
    }
}
